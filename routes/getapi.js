'use strict';


// We'll use it just to launch main async function
const Promise = require('bluebird');

// Async request lib
const request = require('request-promise');

// Util lib, used just for pretty object printing
const util = require('util');

// Pretty print function itself, will be used to make pretty output out of API response
const pretty_print = (data, {showHidden = false, depth = null, colors = true} = {}) => {
    return console.log(util.inspect(data, {showHidden, depth, colors}));
};


// Main function (just to use `async`/`await` instead of Promises :p)
async function main(character_list) {
    let options;
    let response;

    // Getting the character IDs from the given names
    options = {
        method: 'POST',
        uri: 'https://esi.tech.ccp.is/latest/universe/ids/',
        body: character_list,
        json: true,
    };
    try {
        response = await request(options);
        console.log(response);  // NOTE: uncomment to see what we get here
    } catch (e) {
        console.error(e.message);
        return;
    }

    // Form the {character_id: character_name} table
    const characters = {};
    for (let item of response.characters) {
        characters[item.id] = {name: item.name};
    }

    // Get and print battle statistics
    console.log();
    for (let id in characters) {
        options = {
            method: 'GET',
            uri: `https://zkillboard.com/api/stats/characterID/${id}/`
        }
        try {
            response = await request(options);
            console.log(characters[id].name);
            pretty_print(JSON.parse(response));
            console.log();
        } catch (e) {
            console.error(e.message);
            return;
        }
    }
}

// Define input and launch the main function
const input = ['Suu Itashi', 'Man Brown', 'Miyafugi Yoshiko'];

Promise.resolve()
    .then(() => main(input));

