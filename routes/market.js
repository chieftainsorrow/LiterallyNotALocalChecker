"use strict";
const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
const fs = require('fs');
const request = require('request-promise');
const util = require('util');
router.use(bodyParser.json());
const pretty_print = (data, {showHidden = false, depth = null, colors = true} = {}) => {
    return console.log(util.inspect(data, {showHidden, depth, colors}));
};

async function main () {
    let options;
    let response;
    options = {
        method: 'GET',
        uri: 'https://esi.tech.ccp.is/latest/markets/prices/?datasource=tranquility',
        json: true
    }
    try{
        response = await request(options);
        console.log('Market data:');
        pretty_print(response);
    } catch (e) {
        console.error(e.message);
        return ;
    }
    console.log('Is is work?');
    //Save market data into file, format it before save bcase fs cannot save pure response from eve api
    fs.writeFileSync('./marketData.js',`const marketData = ${util.inspect(response, {showHidden: false, depth:null, colors:false, maxArrayLength: null})}; \nmodule.exports = marketData;`);
    console.log('I believe it works now');
}


//util.inspect(response, {showHidden: false, depth:null, colors:false, maxArrayLength: null})
router.get('/', async function (req, res) {
return main();
});

module.exports = router;