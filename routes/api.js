'use strict';
const express = require('express');
const router = express.Router();
const bodyParser = require('body-parser');
// We'll use it just to launch main async function
const promise = require('bluebird');
// Async request lib
const request = require('request-promise');
// Util lib, used just for pretty object printing
const util = require('util');
let alliance_group = {};
const roles = require('../public/javascripts/roles');
const smarts = require('../public/javascripts/smarts');
let recons = {};
let logis = {};
let hic = {};
let dic = {};
let interceptors = {};
let smart_bomb_arr = {};
let cyno_data = {};
let km_data = {};
let km_cyno_list = {};
router.use(bodyParser.json());


// Pretty print function itself, will be used to make pretty output out of API response
const pretty_print = (data, {showHidden = false, depth = null, colors = true} = {}) => {
    return console.log(util.inspect(data, {showHidden, depth, colors}));
};
const float_helper = (number, n = 3) => {
    number = parseFloat(number).toFixed(n);
    return parseFloat(number);

};

// Main function (just to use `async`/`await` instead of Promises :p)
async function main(character_list) {
    let options;
    let response;

    console.log('First test message before id will be fetched');
    // Getting the character IDs from the given names
    options = {
        method: 'POST',
        uri: 'https://esi.tech.ccp.is/latest/universe/ids/',
        body: character_list,
        json: true,
    }
    try {
        response = await request(options);
        console.log('EVE API characters response:');
        pretty_print(response);  // NOTE: uncomment to see what we get here
    } catch (e) {
        console.error(e.message);
        return;
    }

    // Form the {character_id: character_name} table
    const characters = {};
    for (let item of response.characters) {
        characters[item.id] = {name: item.name};
    }
    console.log('test');
    console.log(characters);

    // Get and print battle statistics

    for (let id in characters) {
        options = {
            method: 'GET',
            uri: `https://zkillboard.com/api/stats/characterID/${id}/`,
            headers: {
                'User-Agent': 'local-checker',
            }
        }
        try {
            response = await request(options);
            //console.log(characters[id].name);
            console.log(`\nZKB response for ${characters[id].name}:`);
            pretty_print(JSON.parse(response));
        } catch (e) {
            console.error(e.message);
            return;
        }
        let zkbdata = JSON.parse(response);

        characters[id].gang_ratio = zkbdata.gangRatio;
        characters[id].efficiency = {
            isk: {
                destroyed: float_helper((zkbdata.iskDestroyed / 1000000000), 2),
                lost: float_helper((zkbdata.iskLost / 1000000000), 2)
            },
            ships: {
                destroyed: zkbdata.shipsDestroyed,
                lost: zkbdata.shipsLost
            },
            solo: {
                destroyed: zkbdata.soloKills,
                lost: zkbdata.soloLosses
            }
        }

        characters[id].top_ships = []; //Key value can be empty array
        for (let i = 0; i < 3; i++) {

            characters[id].top_ships.push(zkbdata.topAllTime[4].data[i].shipName); //Adding elements into empty array via cycle
        }

        //Get Corporation Name
        options = {
            method: 'GET',
            uri: `https://esi.tech.ccp.is/latest/corporations/${zkbdata.info.corporationID}/`
        }
        try {
            response = await request(options);
            console.log('\nEVE API corporation response:');
            pretty_print(JSON.parse(response));
        } catch (e) {
            console.error(e.message);
            return;
        }
        let corp_data = JSON.parse(response);
        characters[id].corporation = corp_data.name;
        //Get Alliance name
        if (zkbdata.info.allianceID === 0) { //If Alliance Name doesn't exist return 'No Alliance'
            characters[id].alliance = 'No alliance';
        }
        else {
            options = {
                method: 'GET',
                uri: `https://esi.tech.ccp.is/latest/alliances/${zkbdata.info.allianceID}/`
            }
            try {
                response = await request(options);
                console.log(JSON.parse(response));
            } catch (e) {
                console.error(e.message);
                return;
            }
            let ally_data = JSON.parse(response);
            characters[id].alliance = ally_data.name;
        }
        //Custom avatars
        if (id === '96082499') {
            characters[id].portrait = 'https://i.imgur.com/HNfsA2r.jpg';
        }
        else {
            characters[id].portrait = `http://image.eveonline.com/Character/${id}_256.jpg`
        }

        //Sort by Alliance and corporation's
        if (typeof alliance_group[characters[id].alliance] === "undefined") {
            alliance_group[characters[id].alliance] = {};
            console.log('First If');
            console.log(alliance_group);
        }
        if (typeof alliance_group[characters[id].alliance][characters[id].corporation] === "undefined") {
            alliance_group[characters[id].alliance][characters[id].corporation] = {};
            console.log('Second If');
            console.log(alliance_group);
        }
        else {
            console.log('Alarm!');
        }
        alliance_group[characters[id].alliance][characters[id].corporation][id] = characters[id].name;
        characters.ally_data = alliance_group;
        console.log(id);

        //Get 1month history per char, test try
        let data_options = {year: 'numeric', month: '2-digit', day: '2-digit'};
        let before_date = new Date(Date.now());
        let end_time = new Date(new Date().setDate(before_date.getDate())).toLocaleDateString('ko-KR', data_options).replace(/[^0-9]/g, '');
        let start_time = new Date(new Date().setDate(before_date.getDate() - 30)).toLocaleDateString('ko-KR', data_options).replace(/[^0-9]/g, '');
        console.log('\nDebugging time');
        console.log(before_date);
        console.log(end_time);
        console.log(start_time);

        options = {
            method: 'GET',
            uri: `https://zkillboard.com/api/kills/characterID/${id}/startTime/${start_time}1200/endTime/${end_time}1200/`,
            headers: {
                'User-Agent': 'local-checker',
            }

        }
        try {
            response = await request(options);
            console.log('\nZKB API 1month kills stats response:');
            //pretty_print(JSON.parse(response));
        } catch (e) {
            console.error(e.message);
            return;
        }

        //Get special data from killmails
        let killmails = JSON.parse(response);
        console.log('start killmail parsing');
        console.log(id);
        for (let x in killmails) {
            for (let y in killmails[x].attackers) {
                if (killmails[x].attackers[y].character_id == id) {
                    if (roles[killmails[x].attackers[y].ship_type_id] == 'recon') {
                        recons[killmails[x].killmail_id] = 'Warning! Recon alert!';
                        //console.log('\nRecon Alert!');
                    }
                    if (roles[killmails[x].attackers[y].ship_type_id] == 'logi_frigate') {
                        logis[killmails[x].killmail_id] = 'Warning! Logistic frigate alert!';
                        //console.log('\nLogi frigate alert!');
                    }
                    if (roles[killmails[x].attackers[y].ship_type_id] == 'logist') {
                        logis[killmails[x].killmail_id] = 'Warning! Logistic alert!';
                        //console.log('\nLogist alert!');
                    }
                    if (roles[killmails[x].attackers[y].ship_type_id] == 'inty') {
                        interceptors[killmails[x].killmail_id] = 'Warning! Inty alert!';
                        //console.log('\nLogist alert!');
                    }
                    if (roles[killmails[x].attackers[y].ship_type_id] == 'interdictor') {
                        dic[killmails[x].killmail_id] = 'Warning! Dictor alert!';
                    }
                    if (roles[killmails[x].attackers[y].ship_type_id] == 'HIC') {
                        hic[killmails[x].killmail_id] = 'Warning! HIC alert!';
                    }

                    if (smarts[killmails[x].attackers[y].weapon_type_id] == 'smart_bomb') {
                        smart_bomb_arr[killmails[x].killmail_id] = 'Warning! Smartbomb faggot on the way';
                        //console.log('\nTest Smart Bomb Alert!');
                    }
                }
            }
        }
        console.log('\nTesting killmail obj');
        console.log((Object.keys(recons).length) / (Object.keys(killmails).length));
//Get special data from lossmailss
        options = {
            method: 'GET',
            uri: `https://zkillboard.com/api/losses/characterID/${id}/startTime/${start_time}1200/endTime/${end_time}1200/`,
            headers: {
                'User-Agent': 'local-checker',
            }

        }
        try {
            response = await request(options);
            console.log('\nZKB API 1month losses stats response:');
            //pretty_print(JSON.parse(response));
        } catch (e) {
            console.error(e.message);
            return;
        }
        let lossmails = JSON.parse(response);
        for (let x in lossmails) {
                if (roles[lossmails[x].victim.ship_type_id] != 'noobship') { //Если не нубшип, то
                    if (roles[lossmails[x].victim.ship_type_id] != 'bomber') { //Если не бобер, то
                        for (let z in lossmails[x].victim.items) { //Проходимся по фиту жертвы, если зафичена цинка, то
                            if (lossmails[x].victim.items[z].item_type_id === 21096) { //Записываем в массив
                                cyno_data[lossmails[x].killmail_id] = 'Warning! Cynobait ahead!'; //Номер килла и предупреждение, что цина.
                            }
                            if (lossmails[x].victim.items[z].item_type_id === 28646) { //Если зафичена ковроцина, то записываем в цинодату.
                                cyno_data[lossmails[x].killmail_id] = 'Warning! Covertcyno ahead!';
                            }
                        }
                    }
                    if (roles[lossmails[x].victim.ship_type_id] == 'bomber') { //Если наш пилот сидит  на бобре, то
                        for (let z in lossmails[x].victim.items) {
                            if (lossmails[x].victim.items[z].item_type_id === 21096) {
                                cyno_data[lossmails[x].killmail_id] = 'Warning! Bomber with cyno!'; //Номер килла и предупреждение, что цина.
                            }
                            if (lossmails[x].victim.items[z].item_type_id === 28646) { //Если зафичена ковроцина, то записываем в цинодату.
                                cyno_data[lossmails[x].killmail_id] = 'Warning! Bomber with covert cyno!';
                            }

                        }


                    }
                    if (roles[lossmails[x].victim.ship_type_id] == 't3c') {
                        for (let z in lossmails[x].victim.items) {
                            if (lossmails[x].victim.items[z].item_type_id === 21096) {
                                cyno_data[lossmails[x].killmail_id] = 'Warning! T3 hunter ahead!'; //Номер килла и предупреждение, что цина.
                            }
                            if (lossmails[x].victim.items[z].item_type_id === 28646) { //Если зафичена ковроцина, то записываем в цинодату.
                                cyno_data[lossmails[x].killmail_id] = 'Warning! T3 hunter ahead!';
                            }

                        }
                    }
                }


        }


//add bomber and t3c losses data into object
        for (let x in lossmails) {
            if (cyno_data[x] == 'Warning! T3 hunter ahead!') {
                km_cyno_list[cyno_data[x]] = 'Warning! T3 hunter ahead!';
            }
            if (cyno_data[x] == 'Warning! Bomber with cyno!') {
                km_cyno_list[cyno_data[x]] = 'Warning! Bomber with cyno';

            }
        }

        //add killmails and lossmails data into character[id] object
        km_data.recons = ((Object.keys(recons).length) / (Object.keys(killmails).length)).toFixed(2);
        km_data.interceptors = ((Object.keys(interceptors).length) / (Object.keys(killmails).length)).toFixed(2);
        km_data.hic = ((Object.keys(hic).length) / (Object.keys(killmails).length)).toFixed(2);
        km_data.dic = ((Object.keys(dic).length) / (Object.keys(killmails).length)).toFixed(2);
        km_data.logis_kill = ((Object.keys(logis).length) / (Object.keys(killmails).length)).toFixed(2);
        km_data.logis_loss = ((Object.keys(logis).length) / (Object.keys(lossmails).length)).toFixed(2);
        km_data.cyno_prob = ((Object.keys(cyno_data).length) / (Object.keys(killmails).length)).toFixed(2);
        km_data.smarts = ((Object.keys(smart_bomb_arr).length) / (Object.keys(killmails).length)).toFixed(2);
        console.log(km_data);

        characters[id].killmails_analyze = km_data;
        characters[id].cyno_list = km_cyno_list;


    }
    console.log('\nProcessed data:');
    pretty_print(characters);
    pretty_print(alliance_group);

    return characters;


}


router.all('/', async function (req, res) {
    const data = await main(req.body);
    res.json(data);
});

module.exports = router;