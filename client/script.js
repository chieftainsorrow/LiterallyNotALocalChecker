var app = new Vue({
    el: '#app',
    data: {
        characters: "",
        api_data: null,
        warning: ""
    },
    methods: {
        get_data: function () {
            if (this.characters !== "") {
                const post_data = this.characters.split('\n');
                console.warn(post_data);
                // TODO: fetch data from the backend
                axios.post(`https://esi.tech.ccp.is/latest/universe/ids/`, post_data)
                    .then(response => {
                        console.log(response);
                        this.api_data = response.data;
                        this.warning = "";
                    })
                    .catch(error => {
                        this.warning = error;
                        console.error(error);
                    });
            }
            else {
                this.warning = "please paste characters first";
            }
        }
    }
    
});
